;;;; This software is part of the SBCL system.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.
(in-package :sandbox)

(define-condition format-error (error)
  ((complaint :reader format-error-complaint :initarg :complaint)
   (args :reader format-error-args :initarg :args :initform nil)
   (control-string :reader format-error-control-string
                   :initarg :control-string)
   (offset :reader format-error-offset :initarg :offset)
   (second-relative :reader format-error-second-relative
                    :initarg :second-relative :initform nil)
   (print-banner :reader format-error-print-banner :initarg :print-banner
                 :initform t))
  (:report %print-format-error))

(defun format-error* (complaint args &rest initargs &key &allow-other-keys)
  (apply #'error 'format-error :complaint complaint :args args initargs))

(defun format-error (complaint &rest args)
  (format-error* complaint args))

(defun %print-format-error (condition stream)
  (format stream
          "~:[~*~;error in ~S: ~]~?~@[~%  ~A~%  ~V@T^~@[~V@T^~]~]"
          (format-error-print-banner condition)
          'format
          (format-error-complaint condition)
          (format-error-args condition)
          (format-error-control-string condition)
          (format-error-offset condition)
          (format-error-second-relative condition)))

(defun format-error-at* (control-string offset complaint args
                         &rest initargs &key &allow-other-keys)
  (apply #'error 'format-error
         :complaint complaint :args args
         :control-string control-string
         :offset offset
         initargs))

(defun format-error-at (control-string offset complaint &rest args)
  (format-error-at* control-string offset complaint args))

(defun parse-directive (string start)
  (let ((posn (1+ start)) (params nil) (colonp nil) (atsignp nil)
        (end (length string)))
    (flet ((get-char ()
             (if (= posn end)
                 (format-error-at string start
                                  "String ended before directive was found")
                 (schar string posn)))
           (check-ordering ()
             (when (or colonp atsignp)
               (format-error-at*
                string posn
                "Parameters found after #\\: or #\\@ modifier" '()))))
      (loop
       (let ((char (get-char)))
         (cond ((or (char<= #\0 char #\9) (char= char #\+) (char= char #\-))
                (check-ordering)
                (multiple-value-bind (param new-posn)
                    (parse-integer string :start posn :junk-allowed t)
                  (push (cons posn param) params)
                  (setf posn new-posn)
                  (case (get-char)
                    (#\,)
                    ((#\: #\@)
                     (decf posn))
                    (t
                     (return)))))
               ((or (char= char #\v)
                    (char= char #\V))
                (check-ordering)
                (push (cons posn :arg) params)
                (incf posn)
                (case (get-char)
                  (#\,)
                  ((#\: #\@)
                   (decf posn))
                  (t
                   (return))))
               ((char= char #\#)
                (check-ordering)
                (push (cons posn :remaining) params)
                (incf posn)
                (case (get-char)
                  (#\,)
                  ((#\: #\@)
                   (decf posn))
                  (t
                   (return))))
               ((char= char #\')
                (check-ordering)
                (incf posn)
                (push (cons posn (get-char)) params)
                (incf posn)
                (unless (char= (get-char) #\,)
                  (decf posn)))
               ((char= char #\,)
                (check-ordering)
                (push (cons posn nil) params))
               ((char= char #\:)
                (if colonp
                    (format-error-at*
                     string posn "Too many colons supplied" '())
                    (setf colonp t)))
               ((char= char #\@)
                (if atsignp
                    (format-error-at*
                     string posn "Too many #\\@ characters supplied" '())
                    (setf atsignp t)))
               (t
                (when (and (char= (schar string (1- posn)) #\,)
                           (or (< posn 2)
                               (char/= (schar string (- posn 2)) #\')))
                  (check-ordering)
                  (push (cons (1- posn) nil) params))
                (return))))
       (incf posn))
      (let ((char (get-char)))
        (cond ((char= char #\/)
               (let ((closing-slash (position #\/ string :start (1+ posn))))
                 (if closing-slash
                     (setf posn closing-slash)
                     (format-error-at string posn "No matching closing slash"))
                 (extract-user-fun-name string start (1+ posn))))
              ((char= char #\?)
               (not-allowed "~?"))
              ((char= #\{)
               (let ((next (position #\~ string :start (1+ posn))))
                 (when (eql next (1+ posn))
                   (when (char= (nth-value 1 (parse-directive string next)) #\})
                     (not-allowed "~{~}"))))))
        (values (1+ posn) char)))))

(defun extract-user-fun-name (string start end)
  ;; Searching backwards avoids finding the wrong slash in a funky string
  ;; such as "~'/,'//fun/" which passes #\/ twice to FUN as parameters.
  (let* ((slash (or (position #\/ string :start start :end (1- end)
                              :from-end t)
                    (format-error "Malformed ~~/ directive")))
         (name (nstring-upcase (subseq string (1+ slash) (1- end))))
         (first-colon (position #\: name))
         (second-colon (if first-colon (position #\: name :start (1+ first-colon))))
         (symbol
          (cond ((and second-colon (= second-colon (1+ first-colon)))
                 (subseq name (1+ second-colon)))
                (first-colon
                 (subseq name (1+ first-colon)))
                (t name)))
         (package
            (if (not first-colon)
                "COMMON-LISP-USER"
                (subseq name 0 first-colon)))
         (symbol (or (sandbox-find-symbol symbol package)
                     (sandbox-intern symbol package))))
    (or (filter-function symbol)
        (error 'not-allowed :name symbol))))

(defun check-control-string (string)
  (declare (simple-string string))
  (let ((index 0))
    (loop
     (let ((next-directive (position #\~ string :start index)))
       (unless next-directive
         (return))
       (setf index (parse-directive string next-directive))))))
