(asdf:defsystem #:sandbox
  :serial t
  :depends-on ("eclector")
  :components ((:file "packages")
               (:file "portable-sb-walk")
               (:file "loop")
               (:file "walk-macros")
               (:file "format")
               (:file "sandbox"))
  :in-order-to ((test-op (test-op :sandbox/test))))

(asdf:defsystem #:sandbox/test
  :serial t
  :depends-on (sandbox rt)
  :components ((:file "tests"))
  :perform (test-op (o c)
                    (let ((*package* (find-package :sandbox-tests)))
                      (funcall (intern (symbol-name :do-tests) (find-package :rt))))))
