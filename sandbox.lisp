(in-package #:sandbox)

(defvar *package* nil)
(defvar *allowed-packages* (list
                            (list (find-package :cl)
                                  :disallow '(delete-file rename-file
                                              probe-file truename
                                              ;; Packages
                                              defpackage
                                              make-package
                                              delete-package
                                              rename-package
                                              export import unexport
                                              use-package unuse-package
                                              shadow shadowing-import
                                              find-all-symbols
                                              do-symbols do-external-symbols do-all-symbols
                                              package-shadowing-symbols
                                              ;; restarts
                                              invoke-restart
                                              invoke-restart-interactively
                                              use-value
                                              store-value
                                              ;;
                                              function-lambda-expression))
                            (list (find-package :eclector.reader)
                                  :allow '(eclector.reader:quasiquote
                                           eclector.reader:unquote
                                           eclector.reader:unquote-splicing))))

(defun load (file))

(defclass client ()
  ())

(defparameter *replace-symbols*
  '(cl:*package* *package*
    cl:satisfies satisfies))

(defmethod eclector.reader:interpret-symbol ((client client) stream package-name symbol-name intern)
  (let ((package (if (eq package-name :current)
                     cl:*package*
                     (or (find-package package-name)
                         (error "No package named ~a" package-name)))))
    (unless (or (eq package-name :current)
                (eq package-name :keyword)
                (member package *allowed-packages* :test #'eq :key #'car))
      (error "Can't use symbols from ~a" package))
    (multiple-value-bind (symbol status) (find-symbol symbol-name package)
      (if status
          symbol
          (cond (intern
                 (when (not (or (eq package-name :keyword)
                                (eq package-name :current)))
                   (error "Can't intern ~a into ~a" symbol-name package-name))
                 (if (eq package-name :keyword)
                     (intern symbol-name :keyword)
                     (intern symbol-name)))
                (t
                 (error "Symbol ~S not found in the ~a package." symbol-name (package-name package))))))))

(defmethod eclector.reader:evaluate-expression  ((client client) expression)
  (eval expression))

(defun read-from-string (string)
  (let ((eclector.reader:*client* (make-instance 'client)))
    (eclector.reader:read-from-string string)))

(defun symbol-allowed-p (symbol)
  (let* ((symbol-package (symbol-package symbol))
         (package (find (symbol-package symbol) *allowed-packages* :key #'car :test #'eq)))
    (or (eq symbol-package cl:*package*)
        (and package
             (let ((allow (getf (cdr package) :allow))
                   (disallow (getf (cdr package) :disallow)))
               (unless (or allow disallow)
                 (error "Neither :allow nor :disallow for ~a" (package-name (car package))))
               (if allow
                   (member symbol allow)
                   (not (member symbol disallow))))))))

(defparameter *replace-functions*
  '(funcall sandbox-funcall
    apply sandbox-apply
    cl:eval eval
    symbol-function sandbox-symbol-function
    fdefinition sandbox-symbol-function
    find-symbol sandbox-find-symbol
    intern sandbox-intern
    unintern sandbox-unintern
    makunbound sandbox-makunbound
    fmakunbound sandbox-fmakunbound
    format sandbox-format
    formatter sandbox-formatter
    error sandbox-error
    cerror sandbox-cerror
    signal sandbox-signal
    warn sandbox-warn
    make-condition sandbox-make-condition
    disassemble sandbox-disassemble
    open sandbox-open))

(defun sandbox-intern (symbol-name &optional (package cl:*package*))
  (when (not (or (eq package :keyword)
                 (eq package (find-package :keyword))
                 (eq package cl:*package*)))
    (error "Can't intern ~a into ~a" symbol-name package))
  (intern symbol-name package))

(defun sandbox-find-symbol (symbol-name &optional (package cl:*package*))
  (let ((package (or (find-package package)
                     (error "No package named ~a" package))))
    (unless (member package *allowed-packages* :test #'eq :key #'car)
      (error "Can't use symbols from ~a" package))
    (multiple-value-bind (symbol status) (find-symbol symbol-name package)
      (if status
          symbol
          (error "Symbol ~S not found in the ~a package." symbol-name (package-name package))))))

(defun filter-function (name)
  (if (functionp name)
      name
      (and (typecase name
             (symbol
              (symbol-allowed-p name))
             ((cons (eql setf) (cons symbol null))
              (symbol-allowed-p (cadr name))))
           (or (getf *replace-functions* name)
               name))))

(define-condition not-allowed (error)
  ((name :initarg :name
         :initform nil
         :accessor not-allowed-name))
  (:report
   (lambda (condition stream)
     (format stream
             "Not allowed to use ~s"
             (not-allowed-name condition)))))

(define-condition file-not-allowed (not-allowed)
  ((file :initarg :file
         :initform nil
         :accessor not-allowed-file)
   (direction :initarg :direction
              :initform nil
              :accessor not-allowed-direction))
  (:report
   (lambda (condition stream)
     (format stream
             "Not allowed to use ~s on ~s with :DIRECTION ~s"
             (not-allowed-name condition)
             (not-allowed-file condition)
             (not-allowed-direction condition)))))

(defun sandbox-symbol-function (name)
  (let ((fun (filter-function name)))
    (if fun
        (symbol-function fun)
        (lambda (&rest args)
          (declare (ignore args))
          (error 'not-allowed :name name)))))

(defun sandbox-funcall (function &rest args)
  (let ((fun (filter-function function)))
    (if fun
        (apply #'funcall fun args)
        (error 'not-allowed :name function))))

(defun sandbox-apply (function &rest args)
  (let ((fun (filter-function function)))
    (if fun
        (apply #'apply fun args)
        (error 'not-allowed :name function))))

(defun not-allowed (function &rest args)
  (declare (ignore args))
  (error 'not-allowed :name function))

(defun sandbox-format (destination control-string &rest arguments)
  (check-control-string control-string)
  (apply #'format destination control-string
         arguments))

(defun sandbox-error (datum &rest arguments)
  (check-control-string datum)
  (apply #'error datum arguments))

(defvar *allowed-files* nil)

(defun sandbox-open (filename &rest args &key (direction :input) &allow-other-keys)
  (let* ((*default-pathname-defaults* #P"/")
         (pathname (merge-pathnames filename))
         (allowed (assoc pathname *allowed-files* :test #'equal)))
    (if (case (cadr allowed)
          (:io t)
          (:input (eq direction :input))
          (:output (eq direction :output)))
        (apply #'open pathname args)
        (error 'file-not-allowed :name 'open
                                 :file pathname :direction direction))))

(defun wrap-call (function)
  (or (filter-function function)
      (lambda (&rest args)
        (declare (ignore args))
        (error 'not-allowed :name function))))

(defun replace-symbols (form)
  (typecase form
    (symbol
     (getf *replace-symbols* form form))
    (cons
     (cons (replace-symbols (car form))
           (replace-symbols (cdr form))))
    (t form)))

(deftype satisfies (&whole whole predicate)
  `(cl:satisfies
    ,(or (filter-function predicate)
         (error 'not-allowed :name whole))))

(defun prepare-form (form)
  (replace-symbols
   (portable-sb-walker:walk-form
    form
    nil
    (lambda (subform context env)
      (if (eq context :call)
          (values `(wrap-call
                    ,(portable-sb-walker::walk-form-internal subform :eval env))
                  t)
          (if (consp subform)
              (let ((name (car subform)))
                (cond ((eq name 'function)
                       (if (typep (cadr subform) '(cons (eql lambda)))
                           subform
                           (let ((fun (filter-function (cadr subform))))
                             (if fun
                                 (portable-sb-walker:relist subform 'function fun)
                                 (values `(lambda (&rest args)
                                            (not-allowed ',(cadr subform) args))
                                         t)))))
                      ((or (not (symbolp name))
                           (symbol-allowed-p name))
                       (let ((replace (getf *replace-functions* name)))
                         (if replace
                             (values (cons replace
                                           (portable-sb-walker::walk-repeat-eval
                                            (cdr subform) env))
                                     t)
                             subform)))
                      (t
                       (values (list 'not-allowed
                                     (list 'quote name)
                                     (list 'quote (cdr subform)))
                               t))))
              subform))))))

(defun sandbox-compile (form)
  (cl:eval
   `(cl:compile
     nil
     (lambda ()
       ,(prepare-form form)))))

(defun eval (form)
  (let ((cl:*package* (or *package*
                          cl:*package*)))
    (cl:eval
     (prepare-form form))))
