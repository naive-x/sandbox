(defpackage :sandbox-tests
  (:use :cl :rt))

(in-package :sandbox-tests)

(deftest let
    (sandbox:eval '(let* ((open 10)) open))
  10)
(defmacro raises-error-p (&body body)
  `(handler-case (progn ,@body nil)
     (sandbox:not-allowed () t)))

(deftest let
    (sandbox:eval '(let* ((open 10)) open))
  10)

(deftest let.2
    (raises-error-p (sandbox:eval '(let* ((open (open "a"))) open)))
  t)


(deftest let.3
    (raises-error-p (sandbox:eval '(let* ((open "a")) (open open))))
  t)

(deftest defclass
    (progn
      (sandbox:eval '(defclass foo () ((a :initarg :a :initform (open "a")))))
      (raises-error-p (sandbox:eval '(make-instance 'foo))))
  t)

(deftest defclass.2
    (progn
      (sandbox:eval '(defclass bar (foo) () (:default-initargs :a (open "a"))))
      (raises-error-p (sandbox:eval '(make-instance 'bar))))
  t)

(deftest defmethod
    (raises-error-p (sandbox:eval '(defmethod m ((x (eql (open "a")))))))
  t)


(deftest defstruct 
    (progn
      (sandbox:eval '(defstruct struct.1 (a (open "a"))))
      (raises-error-p (sandbox:eval '(make-struct.1))))
  t)

(deftest defstruct.2
    (progn
      (sandbox:eval '(defstruct (struct.2 (:constructor make-struct.2 (&optional (a (open "a")))))
                      a))
      (raises-error-p (sandbox:eval '(make-struct.2))))
  t)

(deftest defstruct.3
    (progn
      (sandbox:eval '(defstruct (struct.3 (:print-function open))
                      a))
      (raises-error-p (sandbox:eval '(princ-to-string (make-struct.3)))))
  t)

(deftest defstruct.4
    (progn
      (sandbox:eval '(defstruct (struct.4 (:include struct.3 (a (open "a"))))))
      (raises-error-p (sandbox:eval '(make-struct.4))))
  t)

(deftest function
    (raises-error-p (sandbox:eval '(funcall (function (lambda () (open "a"))))))
  t)

(deftest read-time-eval
    (handler-case (progn (sandbox:read-from-string "#.(open \"/dev/null\")")
                         nil)
      (eclector.reader:read-time-evaluation-error () t))
  t)

(deftest satisfies
    (raises-error-p (sandbox:eval '(typep "a" '(satisfies open))))
  t)

(deftest handler-case
    (raises-error-p (sandbox:eval '(handler-case (open "a"))))
  t)

(deftest handler-case.2
    (raises-error-p (sandbox:eval '(handler-case (error "a") (error () (open "a")))))
  t)

(deftest handler-case.2
    (raises-error-p (sandbox:eval '(handler-case t
                                    (:no-error (x &optional (m (open "a"))) m x))))
  t)

(deftest handler-bind
    (raises-error-p (sandbox:eval '(block
                                    nil
                                    (handler-bind ((error (lambda (c)
                                                            (open "a")
                                                            (return c))))
                                      (error "a")))))
  t)

(deftest handler-bind.2
    (raises-error-p (sandbox:eval '(block
                                    nil
                                    (handler-bind ((error (progn 'probe-file)))
                                      (error "a")))))
  t)

(deftest restarts
    (raises-error-p (sandbox:eval '(handler-bind ((error (lambda (c) (use-value 'open c))))
                                    (error "/dev/null"))))
  t)

(deftest format
    (raises-error-p (sandbox:eval '(format t "~/cl:probe-file/" "a")))
  t)

(deftest format.2
    (raises-error-p (sandbox:eval '(format t "~?" "~/cl:probe-file/" '("a"))))
  t)

(deftest format.3
    (raises-error-p (sandbox:eval '(format t "~1{~:}" "~/cl:probe-file/" '("a"))))
  t)

(deftest loop
    (raises-error-p (sandbox:eval '(loop for i in (open "a"))))
  t)

(deftest loop
    (raises-error-p (sandbox:eval '(loop for x = (open "a"))))
  t)

(deftest defun
    (progn
      (sandbox:eval '(defun fun () 10))
      (sandbox:eval '(fun)))
  10)

(deftest defun.2
    (progn
      (sandbox:eval '(defun fun.2 () (open "a")))
      (raises-error-p (sandbox:eval '(fun.2))))
  t)

(deftest defmacro
    (progn
      (sandbox:eval '(defmacro macro () 10))
      (sandbox:eval '(macro)))
  10)

(deftest defmacro.2
    (progn
      (sandbox:eval '(defmacro macro.2 () '(open "a")))
      (raises-error-p (sandbox:eval '(macro.2))))
  t)

(deftest defmacro.3
    (progn
      (sandbox:eval '(defmacro macro.3 () (open "a") nil))
      (raises-error-p (sandbox:eval '(macro.3))))
  t)

(deftest macrolet
    (sandbox:eval '(macrolet ((x () 10)) (x)))
  10)



(deftest macrolet.2
    (raises-error-p (sandbox:eval '(macrolet ((x () '(open "a"))) (x))))
  t)

(deftest macrolet.3
    (raises-error-p (sandbox:eval '(macrolet ((x () (open "a") nil)) (x))))
  t)

(deftest flet
    (sandbox:eval '(flet ((x () 10)) (x)))
  10)

(deftest flet.2
    (raises-error-p (sandbox:eval '(flet ((x () (open "a") nil)) (x))))
  t)

(deftest open
  (let ((sandbox:*allowed-files* '((#p"/dev/null" :input))))
    (sandbox:eval '(with-open-file (str "/dev/null") (streamp str))))
  t)

(deftest open.2
  (let ((sandbox:*allowed-files* '((#p"/dev/null" :io))))
    (sandbox:eval '(with-open-file (str "/dev/null" :direction :output :if-exists :append)
                    (streamp str))))
  t)
