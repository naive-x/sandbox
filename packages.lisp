(defpackage "PORTABLE-SB-WALKER"
  (:use "CL")
  (:export "DEFINE-WALKER-TEMPLATE"
           "WALK-FORM"
           "*WALK-FORM-EXPAND-MACROS-P*"
           "VAR-LEXICAL-P" "VAR-SPECIAL-P"
           "VAR-GLOBALLY-SPECIAL-P"
           "VAR-DECLARATION"
           "RECONS"
           "RELIST"
           "RELIST*"))

(defpackage #:sandbox
  (:use :cl)
  (:shadow read-from-string eval load
           *package* satisfies)
  (:export read-from-string eval load
           sandbox-compile
           *package*
           #:not-allowed-name
           #:not-allowed
           #:not-allowed-file
           #:file-not-allowed
           #:*allowed-files*))
